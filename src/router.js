import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Playlist from './views/Playlist.vue'
import Editprofile from './views/Editprofile.vue'
import Search from './views/Search.vue'
import Recentlyplayed from './views/Recentlyplayed.vue'
import Artist from './views/Artist.vue'
import Detailplaylist from './views/Detailplaylist.vue'
import Landing from './views/Landing.vue'
import Register from './views/Register.vue'
import Login from './views/Login.vue'
import Popularsongs from './views/Popularsongs.vue'
import Recommended from './views/Recommended.vue'
import Profile from './views/Profile.vue'
import Addsong from './views/Addsong.vue'
import Changepassword from './views/Changepassword.vue'
import Forgetpassword from './views/Forgetpassword.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: { 
        requiresAuth: true
      }
    },
    {
      path: '/playlist',
      name: 'playlist',
      component: Playlist
    },
    {
      path: '/profile/editprofile',
      name: 'editprofile',
      component: Editprofile
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/recentlyplayed',
      name: 'recentlyplayed',
      component: Recentlyplayed
    },
    {
      path: '/artist',
      name: 'searchartist',
      component: Artist
    },
    {
      path: '/artist',
      name: 'playlistartist',
      component: Artist
    },
    {
      path: '/detailplaylist',
      name: 'detailplaylist',
      component: Detailplaylist
    },
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/popularsongs',
      name: 'popularsongs',
      component: Popularsongs
    },
    {
      path: '/recommended',
      name: 'recommended',
      component: Recommended
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/playlist/addsong',
      name: 'addSong',
      component: Addsong
    },
    {
      path: '/profile/changepassword',
      name: 'changePassword',
      component: Changepassword
    },
    {
      path: '/forgetpassword',
      name: 'forgetpassword',
      component: Forgetpassword
    }
  ]
})

// router.beforeEach((to, from, next) => {
//   if(to.matched.some(record => record.meta.requiresAuth)) {
//     if (this.store.getters.isLoggedIn) {
//       next()
//       return
//     }
//     next('/login') 
//   } else {
//     next() 
//   }
// })

export default router
