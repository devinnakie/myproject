import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    email : localStorage.getItem('user-email') || null,
    id : localStorage.getItem('user-id') || null,
    name : localStorage.getItem('user-name') || null,
    image : localStorage.getItem('user-image') || null,
    isLoggedIn : localStorage.getItem('isLoggedIn') || null,

    dummy_id : localStorage.getItem('dummy-id') || null,
    dummy_text : localStorage.getItem('dummy-text') || null,
    dummy_image : localStorage.getItem('dummy-image') || null
  },
  getters : {
    userEmail: state => state.email,
    userId: state => state.id,
    userName: state => state.name,
    userImage: state => state.image,
    isLoggedIn: state => state.isLoggedIn,
    
    dummy_id: state => state.dummy_id,
    dummy_text: state => state.dummy_text,
    dummy_image: state => state.dummy_image
  },
  mutations: {
    setUser(state, data){
      state.email =  data.email
      state.id = data.id
      state.name = data.name
      state.image = data.image
      state.isLoggedIn = true
    },
    setId(state, data){
      state.dummy_id = data
    },
    setText(state, data){
      state.dummy_text = data
    },
    setImage(state, data){
      state.dummy_image = data
    },
    removeUser(state){
      state.email =  null
      state.id = null
      state.name = null
      state.image = null
      state.isLoggedIn = false
    }
  },
  actions: {
    setUser({commit}, data){
      localStorage.setItem('user-email', data.email)
      localStorage.setItem('user-id', data.id)
      localStorage.setItem('user-name', data.name)
      localStorage.setItem('user-image', data.image)
      localStorage.setItem('isLoggedIn', true)
      commit('setUser', data)
    },
    setId({commit}, data){
      localStorage.setItem('dummy-id', data)
      commit('setId', data)
    },
    setText({commit}, data){
      localStorage.setItem('dummy-text', data)
      commit('setText', data)
    },
    setImage({commit}, data){
      localStorage.setItem('dummy-image', data)
      commit('setImage', data)
    },
    removeUser({commit}){
      localStorage.removeItem('user-email')
      localStorage.removeItem('user-id')
      localStorage.removeItem('user-name')
      localStorage.removeItem('user-image')
      localStorage.setItem('isLoggedIn', false)
      commit('removeUser')
    }
  }
})