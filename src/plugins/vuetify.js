import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    back: '#1b1f3a',
    pinkNeon: '#C8213D',
    purpleNeon:'#9E37F8'
  }
})
