import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import './styles.css'
import axios from 'axios'
import store from './store'

import {Howl, Howler} from 'howler'

Vue.prototype.$http = axios
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

Vue.config.productionTip = false

// adding this to calculate # of the song order
Vue.filter('numbers', (value) => {
  var number = value + 1;
  return number + ". "
})

// adding this to convert numbers for song duration into mins
Vue.filter('minutes', (value) => {
  var min = parseInt(value / 60);
  var sec = parseInt(value % 60);
  min = min < 10 ? "0" + min : min;
  sec = sec <10 ? "0" + sec : sec;
  value = min + ":" + sec;
  return value;
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
